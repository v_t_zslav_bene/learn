#LEARN

##Javascript application for exam revision

###Parts of the application

####Connect the dots
Two collumns of terms are randomly selected from a dataset and the user has to pair them correctly. Mistakes are highlighted. When the user paired all terms correctly, results page appears.

####Memorize the outline
A user is presented with a shuffled outline of a topic and has to put individual entries into the order, in which they would be organized to form a cohesive monologue.

##[demo](http://learn.artbene.cz "Learn application demo")